#include "stdafx.h"
#include "windows.h"
#include <stdio.h>


int main()
{
	WIN32_FIND_DATA f;
	HANDLE h = FindFirstFile("./*", &f);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			puts(f.cFileName);
		} while (FindNextFile(h, &f));
	}
	else
	{
		fprintf(stderr, "Error opening directory\n");
	}
	return 0;
}
